# T9_endava

Automateademoaboutmeform.Formcontainsallthebasicwebelementssoitwillcoverallbasicwebdrivercommands.
Stepstoautomate:
- [ ] 1. Open this link - https://www.techlistic.com/p/selenium-practice-form.html
- [ ] 2. Enter first and lastname(textbox).
- [ ] 3. Select gender(radiobutton).
- [ ] 4. Select years of experience(radiobutton).
- [ ] 5. Enter date.
- [ ] 6. Select profession(checkbox).
- [ ] 7. Select automation tools you are familiar with(multiplecheckboxes).
- [ ] 8. Select continent(selectbox).
- [ ] 9. Select multiple commands from a multiselectbox.
- [ ] 10. Click on submit button.

