using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;

namespace TestProjectEndava
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
            
        }

        [Test]
        public void Test1()
        {
            //Open browser
            IWebDriver webDriver = new ChromeDriver();
            //Navigate to site
            webDriver.Navigate().GoToUrl("https://www.techlistic.com/p/selenium-practice-form.html");

            //Identify textbox
            var firstName = webDriver.FindElement(By.Name("firstname"));
            //Assertion
            Assert.That(firstName.Displayed, Is.True);
            firstName.SendKeys("Melania");

            //Identify textbox
            var lastName = webDriver.FindElement(By.Name("lastname"));
            //Assertion
            Assert.That(lastName.Displayed, Is.True);
            lastName.SendKeys("I");

            //Identify radiobutton
            IWebElement radio = webDriver.FindElement(By.Id("sex-1"));
            //Assertion
            Assert.That(radio.Displayed, Is.True);
            radio.Click();

            //Identify radiobutton
            IWebElement radio1 = webDriver.FindElement(By.Id("exp-0"));
            //Assertion
            Assert.That(radio1.Displayed, Is.True);
            radio1.Click();

            //Identify textbox
            var date = webDriver.FindElement(By.Id("datepicker"));
            //Assertion
            Assert.That(date.Displayed, Is.True);
            date.SendKeys("29.07.2021");

            var element = webDriver.FindElement(By.XPath("//button[@class ='btn btn-info']"));
            element.Click();
            
            

            //IWebElement select = webDriver.FindElement(By.Id("continents"));
            //select.Click();
            /*
             IList<IWebElement> checkbox1 = webDriver.FindElements(By.Name("tool"));
            checkbox1.ElementAt(1).Click();
            //Identify checkbox
            IList<IWebElement> checkbox1 = webDriver.FindElements(By.Name("tool"));
            // This will tell you the number of checkboxes are present
            int Size1 = checkbox1.Count;

            // Start the loop from first checkbox to last checkboxe
            for (int i = 0; i < Size1; i++)
            {
                // Store the checkbox name to the string variable, using 'Value' attribute
                String Value = checkbox1.ElementAt(i).GetAttribute("value");

                // Select the checkbox it the value of the checkbox is same what you are looking for
                if (Value.Equals("Selenium IDE") || Value.Equals("Selenium Webdriver"))
                {
                    checkbox1.ElementAt(i).Click();
                }
            }
            */

            // Kill the browser
            webDriver.Close();

        }
    }
}